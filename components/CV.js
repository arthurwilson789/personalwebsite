import React from 'react'

function CV() {
    return(
        <div>
            <p>Full CV available on request, E-mail me.</p>
            <div>
                <h1>Selected Exhibitions</h1>
                <p>
                    <h2>Thai Bienalle, Korat - 2021</h2>
                    <h2>Taiwan Contemporary Art Museum, Taipei - 2021</h2>
                    <h2>Hong Kong Art Basel, Hong Kong - 2020</h2>
                    <p>Antithesis, with Kachi Chan</p>
                    <h2>BEEP BEEP: The End of the World, Menier Gallery, London - 2021</h2>
                    <p1>Conversations through Copper Pipes</p1>
                    <h2>IRCAM Forum, Centre Pompidou, Paris - 2020</h2>
                    <p>Shellophone, with Susan Atwill and Rahul Pradhan</p>
                    <h2>Networked Music Festival - 2021</h2>
                    <p>Autopia, with Dr. Norah Lorway and Dr. Ed Powley</p>
                    <h2>The VOID Falmouth, Cornwall - 2019</h2>
                    <p>Flowing Points</p>
                    <h2>Fish Factory Penryn, Cornwall - 2019</h2>
                    <p>Kessenyens, with Ed Hanley</p>
                </p>
            </div>
            <div>
                <h1>Publications</h1>
                <h2>Autopia: An AI collaborator for live networked computer music performance</h2>
                <h2>AUTOPIA: An AI Collaborator for Live Coding Music Performances</h2>
                <h2>Autopia: An AI Collaborator for Gamified Live Coding Music Performances</h2>
                <p>An ongoing and developing research project, findings have been published at multiple conferences: AIMC2021, ICLC2020 and AISB2019</p>
            </div>
            <div>
                <h1>Education</h1>
                <h2>MA Information Experience Design, Royal College of Art, London 2019-2021</h2>
                <h2>BA (Hons), Creative Music Technology, Falmouth University, Cornwall</h2>
            </div>
        </div>
    );
}

export default CV;